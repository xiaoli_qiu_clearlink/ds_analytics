# README #

This repo is used to host the analysis the data analytics/data science team has conducted. 



### Prerequisites ###

* Python

### Coding Standards ###

1. Follow [PEP 8](https://www.python.org/dev/peps/pep-0008/) as best as you can.
2. [Best Practices for Airflow](https://airflow.apache.org/docs/1.10.11/best-practices.html)
3. Add comments to make sure the code is readable, understandable and reusable

### Who do I talk to? ###

* For any questions, reach out to the analytics team. 